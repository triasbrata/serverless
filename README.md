## Description

Project ini menggunakan Nestjs + azure serverless function + atlas mongoDB
- [Nestjs](https://nestjs.com/)
- [Nestjs Azure](https://trilon.io/blog/deploy-nestjs-azure-functions)
- [MongoDB Atlas](https://www.mongodb.com/cloud/atlas)
- [Azure Function](https://azure.microsoft.com/en-in/services/functions/)

## Installation

```bash
$ npm install
```

## Setup Envirotment
```bash
cp local.settings.example.json local.settings.json
```
lalu setting variable `URL_MONGODB` dengan url mongo db yang digunakan 

untuk di bagian production di azure serveless bisa di setting lewat vscode dengan `CMD/CTRL + SHIFT + P`  lalu pilih `Azure Function : Add New Setting...` dan masukkan `URL_MONGODB` lalu valuenya dan hasilnya akn seperti berikut
![image](./how-to/assets/add-setting.png)

## Running the app

```bash
# development
$ npm run start:azure

# watch mode
$ npx nest build -w 
```

## Deploy
untuk deploy disini saya menggunakn plugin [Azure Functions](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azurefunctions) di vscode.  step by step-nya bisa mengikuti infomrasi [di sini](https://trilon.io/blog/deploy-nestjs-azure-functions)

## Testing

untuk testing  bisa dengan menggunakan file `test.http` untuk yang local dengan sebelumnya menginstall plugin [RESTClient](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) di vscode.

Untuk melakukan testing app yang telah deploy bisa menggunakan `test-prod.http`


## Development
berikut beberapa refrensi untuk melakukan pengembangan selanjutnya
- [untuk meng-setting header response](./how-to/set-respose-heade.md)
- [membuat module baru](https://docs.nestjs.com/modules)
- [membuat controller controller baru](https://docs.nestjs.com/controllers)
- [membuat service/provider baru](https://docs.nestjs.com/modules)




