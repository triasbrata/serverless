import { Context, HttpRequest } from '@azure/functions';
import { AzureHttpAdapter } from '@nestjs/azure-func-http';
import { createApp } from '../src/main.azure';
const responseHeader = {
  'Content-Type': 'application/json',
};

export default function(context: Context, req: HttpRequest): void {
  context.res.headers = responseHeader;
  AzureHttpAdapter.handle(createApp, context, req);
}
