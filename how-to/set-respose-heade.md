# Setting Respose Header

untuk mensetting response header bisa dengan cara mengedit file `main/index.ts` dan mengubah value dari `responseHeader` dengan notasi object 
contohnya seperti berikut
```javascript
...
const responseHeader = {
  'Content-Type': 'application/json',
  'Content-Encoding':'gzip'
  ...
};
...
```