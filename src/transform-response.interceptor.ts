import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable()
export class TransformResponse implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(data => {
        if (Array.isArray(data?.data)) {
          data.data.map(it => {
            delete it['__v'];
            return it;
          });
        } else if (Reflect.has(data, '__v')) {
          data['__v'] = undefined;
        }
        return data;
      }),
    );
  }
}
