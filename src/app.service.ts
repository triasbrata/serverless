import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Player, PlayerDocument } from './schemas/player.schema';

@Injectable()
export class AppService {
  constructor(
    @InjectModel(Player.name) private playerModel: Model<PlayerDocument>,
  ) {}

  async findPlayer(playerId: string) {
    let player;
    try {
      player = await this.playerModel.findById(playerId);
    } catch (e) {
      throw new BadRequestException('Player not found');
    }
    return player;
  }
  async listPlayer(page: number) {
    const limit = 10;
    const skip = (page - 1 < 0 ? 0 : page - 1) * limit;
    const res = await this.playerModel
      .aggregate()
      .facet({
        metadata: [{ $count: 'total' }],
        data: [{ $skip: skip }, { $limit: limit }],
      })
      .project({
        data: 1,
        total: { $arrayElemAt: ['$metadata.total', 0] },
      })
      .exec();
    return res.pop();
  }
  save(dataDto: Player) {
    return new this.playerModel(dataDto).save();
  }
}
