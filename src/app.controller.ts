import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { AppService } from './app.service';
import { Player } from './schemas/player.schema';

@Controller('player')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getListPlayer(@Query('page', ParseIntPipe) page: number) {
    return this.appService.listPlayer(page);
  }
  @Get('detail/:idPlayer')
  getDetailPlayer(@Param('idPlayer') idPlayer: string) {
    return this.appService.findPlayer(idPlayer);
  }
  @Post()
  async savePlayer(@Body() palyerDto: Player) {
    const playerSaved = await this.appService.save(palyerDto);
    return playerSaved;
  }
}
