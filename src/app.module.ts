import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigKey } from './consts/config.key';
import { PlayerFeature } from './schemas/player.schema';

@Module({
  imports: [
    MongooseModule.forRoot(process.env[ConfigKey.URL_MONGODB]),
    MongooseModule.forFeature([PlayerFeature]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
