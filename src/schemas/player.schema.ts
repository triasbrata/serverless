import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Player {
  @Prop()
  name: string;

  @Prop()
  email: string;

  @Prop()
  nickname: string;
}

export type PlayerDocument = Player & Document;
export const PlayerSchema = SchemaFactory.createForClass(Player);
export const PlayerFeature = { name: Player.name, schema: PlayerSchema };
