import { TransformResponse } from './transform-response.interceptor';

describe('ResposneAsJsonInterceptor', () => {
  it('should be defined', () => {
    expect(new TransformResponse()).toBeDefined();
  });
});
